package com.sy.reggie.controller;

import com.sy.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/employee") // 给前端请求的地址的根路径
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
}
