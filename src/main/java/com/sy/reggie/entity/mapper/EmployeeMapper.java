package com.sy.reggie.entity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sy.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * BaseMapper<Employee> 参数类型是 Employee 实体类
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}
