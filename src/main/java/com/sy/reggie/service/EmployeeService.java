package com.sy.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sy.reggie.entity.Employee;

/**
 *  IService<Employee> 泛型指定实体类
 */
public interface EmployeeService extends IService<Employee> {
}
