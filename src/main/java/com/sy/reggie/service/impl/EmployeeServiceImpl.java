package com.sy.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sy.reggie.entity.Employee;
import com.sy.reggie.entity.mapper.EmployeeMapper;
import com.sy.reggie.service.EmployeeService;
import org.springframework.stereotype.Service;

/**
 * 实现类
 * ServiceImpl<EmployeeMapper,Employee>  类型两个参数，一个mapper一个实体类
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper,Employee> implements EmployeeService {
}
